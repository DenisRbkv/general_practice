import 'package:flutter/material.dart';


class CircleCanvas extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * 0.25,
        height: MediaQuery.of(context).size.height * 0.25,
        child: CustomPaint(
          painter: MyPainter(context),
        ),
      ),
    );
  }
}

class MyPainter extends CustomPainter {
  final BuildContext context;

  MyPainter(this.context);

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true;
    var circleCenter = Offset(MediaQuery.of(context).size.width * 0.125, MediaQuery.of(context).size.height * 0.1);
    paint.color = Colors.indigo;
    canvas.drawCircle(circleCenter, MediaQuery.of(context).size.width * 0.25, paint);
    paint.color = Colors.indigoAccent;
    canvas.drawCircle(circleCenter, MediaQuery.of(context).size.width * 0.22, paint);
    paint.color = Colors.deepOrangeAccent;
    canvas.drawCircle(circleCenter, MediaQuery.of(context).size.width * 0.19, paint);
    paint.color = Colors.yellowAccent;
    canvas.drawCircle(circleCenter, MediaQuery.of(context).size.width * 0.16, paint);
    paint.color = Colors.white;
    canvas.drawCircle(circleCenter, MediaQuery.of(context).size.width * 0.13, paint);
  }

  @override
  bool shouldRepaint(MyPainter old) {
    return false;
  }
}
