import 'package:flutter/material.dart';

import 'circle_canvas.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<int> taskList = List<int>.generate(101, (int index) => index);

  void swapOrder() {
    taskList = taskList.reversed.toList();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: Colors.indigoAccent,
      ),
      body: Stack(
        children: <Widget>[
          CircleCanvas(),
          ListView.builder(
              itemCount: taskList.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                  child: Container(
                    margin: const EdgeInsets.symmetric(horizontal: 20.0),
                    height: 100.0,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.indigoAccent,
                      borderRadius: BorderRadius.circular(20.0),
                      border: Border.all(
                        color: Colors.deepOrangeAccent,
                        width: 4.0,
                      ),
                      gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        colors: [Colors.indigo, Colors.indigoAccent.withOpacity(0.8)],
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          blurRadius: 4.0,
                          spreadRadius: 3.0,
                          offset: Offset(
                            3.0, // horizontal
                            4.0, // vertical
                          ),
                        )
                      ],
                    ),
                    child: Center(
                      child: Text(
                        taskList[index].toString(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                );
              }),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: swapOrder,
        tooltip: 'Increment',
        child: Icon(Icons.swap_vert, size: 40.0),
        backgroundColor: Colors.deepOrangeAccent,
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
